//
//  main.m
//  ControlGastos
//
//  Created by Máster INFTEL 23 on 15/01/14.
//  Copyright (c) 2014 Máster INFTEL 23. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
